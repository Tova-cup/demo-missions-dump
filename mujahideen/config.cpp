#include "macros.hpp"

class CfgPatches {
	class etr_public_mujahideen {
		units[]={};
		weapons[]={};
		requiredVersion=1;
		requiredAddons[]={"etr_public_main"};
		author = AUTHOR;
	};
};

#include "cfgFactionClasses.hpp"
#include "cfgVehicles.hpp"
#include "cfgGroups.hpp"
