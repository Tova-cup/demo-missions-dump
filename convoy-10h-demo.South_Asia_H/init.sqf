hideRoadClutter2={
	params ["_center","_radius"];
	{
		{
			hideObject _x;
		} forEach nearestTerrainObjects [getpos _x, [], 18, false];
	} forEach (_center nearRoads _radius);
};

[[(worldSize / 2), (worldSize / 2), 0], (sqrt 2 * (worldSize / 2))] call hideRoadClutter2;

TOV_fnc_SimpleConvoy = { 
	params ["_convoyGroup",["_convoySpeed",50],["_convoySeparation",50],["_pushThrough", true]];
	if (_pushThrough) then {
		_convoyGroup enableAttack !(_pushThrough);
		{(vehicle _x) setUnloadInCombat [false, false];} forEach (units _convoyGroup);
	};
	_convoyGroup setFormation "COLUMN";
	{
		//(vehicle _x) limitSpeed _convoySpeed*1.15;
		//(vehicle _x) forceSpeed (_convoySpeed*1.15)/3.6;
		(vehicle _x) setConvoySeparation _convoySeparation;
	} forEach (units _convoyGroup);
	(vehicle leader _convoyGroup) limitSpeed _convoySpeed;
	while {sleep 3;true} do {
		{(vehicle _x) setConvoySeparation _convoySeparation;} forEach (units _convoyGroup);
		{
			(vehicle _x) setFuel 1;
			if ((speed vehicle _x < 5) && (_pushThrough || (behaviour _x != "COMBAT"))) then {
				dostop (vehicle _x);
				sleep 0.25;
				(vehicle _x) doFollow (leader _convoyGroup);
				_convoyGroup setFormation "COLUMN";
			};	
		} forEach (units _convoyGroup)-(crew (vehicle (leader _convoyGroup)))-allPlayers; 
	}; 
};

convoyGroup setSpeedMode "NORMAL";

convoyScript = [convoyGroup,100] spawn TOV_fnc_SimpleConvoy;

{
	_x disableAI "RADIOPROTOCOL";
} forEach (units convoyGroup)

/*
{
	_unit=_x;
	_unit disableAI "ALL";
	{_unit enableAI _x} forEach ["MOVE","PATH","ANIM","LIGHTS","TEAMSWITCH","WEAPONAIM","TARGET","AUTOTARGET"];
} forEach allUnits;
*/
/*
TOV_fnc_SimpleConvoy = { 
	params ["_convoyGroup",["_convoySpeed",50],["_convoySeparation",50],["_pushThrough", true]];
	if (_pushThrough) then {
		_convoyGroup enableAttack !(_pushThrough);
		{(vehicle _x) setUnloadInCombat [false, false];} forEach (units _convoyGroup);
	};
	_convoyGroup setFormation "COLUMN";
	{(vehicle _x) limitSpeed _convoySpeed*1.15} forEach (units _convoyGroup);
	(vehicle leader _convoyGroup) limitSpeed _convoySpeed;
	while {sleep 3;true} do { 
		{
			(vehicle _x) setConvoySeparation _convoySeparation;
			(vehicle _x) setFuel 1;
			if ((speed vehicle _x < 5) && (_pushThrough || (behaviour _x != "COMBAT"))) then {
				_initialGroup = group _x;
				dostop (vehicle _x);
				_group = createGroup [west, true];
				[(vehicle _x)] joinsilent _group;
				sleep 0.5;
				[(vehicle _x)] joinsilent _initialGroup;
				(vehicle _x) setConvoySeparation _convoySeparation;
			};	
		} forEach (units _convoyGroup)-(crew (vehicle (leader _convoyGroup)))-[player]; 
	}; 
};


*/